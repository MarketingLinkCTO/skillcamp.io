---
path:    "jordan-hensley"
name:    "Jordan Hensley"
tagline: "Music and Javascript"
avatar:  "../../blog/avatars/jordan.jpg"
role:    "mentor"
---

# Hello!

I am a self taught developer in Raleigh, NC 🇺🇸. I have played around with
several languages, but fell in love with Javascript and the web! I mostly build websites
and applications, but have recently been working on stepping up my design skills!

### I am always happy to help!

Please reach out to me if you have any questions! As a self-taught developer,
I owe a lot to the people on Stackoverflow, Reddit and many other developer
communities. I have always been encouraged and helped by fellow developers,
so I hope pay it back where I can 😄

### Some of my favorite tech

- Javascript & Node.js
- React
- Gatsby
- Firebase

### Some tech that really excites me, and I hope to learn!

- Mobile development (Possibly with React Native)
- Web-components have caught my attention
- I mean, machine learning looks awesome!
- Arduinos / Raspberry Pi's seem like a blast!

### My links

**Website:** <a href="http://thedapperdeveloper.com" target="_blank">The Dapper Developer</a>, a collection of side
projects that almost definitely need to be updated. ¯\\\_(ツ)\_/¯

**LinkedIn:** <a href="http://www.linkedin.com/in/jordanhensley93" target="_blank">@jordanhensley93</a>

### Fun Facts

- I am in a band called The Dapper Conspiracy

<div style="display:flex;flex-direction:row;justify-content:center">
<a href="http://www.youtube.com/watch?feature=player_embedded&v=0nMBj8-Ii84
" target="_blank"><img src="http://img.youtube.com/vi/0nMBj8-Ii84/0.jpg" 
alt="The Dapper Conspiracy" width="240" height="180" border="10" /></a>
</div>

- I am a total music lover, all genres, and I am always looking for great
  new music. Send me your favorite song or artist!

- There is absolutely room in my heart for pineapple on pizza 🍍🍕
