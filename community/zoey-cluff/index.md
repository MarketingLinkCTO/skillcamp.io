---
path: 'zoey-cluff'
name: 'Zoey Cluff'
tagline: 'Not Your Average Gamer Girl'
avatar: 'me.jpeg'
role: 'developer'
---

I've been into tech my whole life. I built my first computer when I was 14, got expelled from high school for accidentally hacking them a few years later. My bad. I've been interested in coding for a while now. I'm fluent in HTML and SCSS, and I'm dangerous enough to code things in Python. I'm still relatively new to JavaScript and hoping to learn. I'm not sure if I want to focus on Frontend or Backend, or a mix of both.

[My Github @zoeycluff](http://github.com/zoeycluff/)
[My LinkedIn @zoeycluff](https://www.linkedin.com/in/zoeycluff/)
