---
path: "simonas-daniliauskas"
name:  "Simonas Daniliauskas"
tagline: "#Software developer"
avatar:  "simonas-daniliauskas.jpg"
role: "developer"
---

# Hi!

I'm currently studying computer science and trying to learn new technologies and skills. I believe that the best way to learn new things is to make something
with other people, so that's why I joined Skillcamp!

### My favorite tech

- C#
- Javascript (Node.js, React)
- Java

### Links

**Website:** <a href="http://simdan.lt" target="_blank">Simdan.lt</a> - porfolio website in lithuanian :)

**Linkedin:** <a href="https://linkedin.com/in/simonas-daniliauskas-3b685a74" target="_blank">@simonas-daniliauskas-3b685a74</a>

**Github:** <a href="https://github.com/simdani" target="_blank">@simdani</a>
