import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'
import transitionTop from '../images/transition_top.svg'

const FooterWrapper = styled.div`
  background: #222222;
  color: #ffffff;
  height: 300px;
  position: relative;
  overflow: hidden;
  padding: 20px 10px;

  @media screen and (max-width: 600px) {
    padding: 0 10px;
  }
`

const Container = styled.div`
  max-width: 1440px;
  height: 100%;
  margin: 0 auto;
  display: flex;
  align-items: flex-end;
  justify-content: flex-end;

  @media screen and (max-width: 600px) {
    flex-direction: column;
    align-items: center;
    justify-content: flex-end;
  }
`

const TransitionTop = styled.img`
  height: 150px;
  position: absolute;
  top: -1px;
  left: 0;
  z-index: 5;
  margin: 0;
  max-width: none;

  @media screen and (max-width: 700px) {
    height: 100px;
  }
`

const StripeBlue = styled.span`
  position: absolute;
  height: 124px;
  width: 425px;
  bottom: 79px;
  left: -200px;
  background-color: #24caf0;
  transform: rotate(-45deg);
  z-index: 2;

  @media screen and (max-width: 700px) {
    height: 75px;
    width: 450px;
    bottom: 145px;
    left: -227px;
  }
`

const StripeYellow = styled.span`
  position: absolute;
  height: 100px;
  width: 648px;
  bottom: 1px;
  left: -244px;
  background-color: #fdc153;
  transform: rotate(-45deg);
  z-index: 2;

  @media screen and (max-width: 700px) {
    height: 70px;
    width: 455px;
    bottom: 94px;
    left: -183px;
  }
`

const StripeRed = styled.span`
  position: absolute;
  height: 100px;
  width: 560px;
  bottom: 40px;
  left: -20px;
  background-color: #f24d47;
  transform: rotate(-45deg);
  z-index: 2;

  @media screen and (max-width: 700px) {
    height: 70px;
    width: 610px;
    bottom: 35px;
    left: -226px;
  }
`

const Copyright = styled.div`
  text-align: right;
  font-style: italic;
  font-size: 12px;
  position: relative;
  z-index: 5;
  padding: 0 15px;

  @media screen and (max-width: 600px) {
    text-align: center;
    padding: 20px;
  }
`

const Menu = styled.div`
  text-align: left;
  font-weight: bold;
  color: white;
  position: relative;
  z-index: 5;
  a:link,
  a:visited,
  a:active {
    padding: 0 15px;
    color: #ffffff;
    text-decoration: none;
  }

  @media screen and (max-width: 600px) {
    text-align: center;
    a:link,
    a:visited,
    a:active {
      padding: 20px 10px;
    }
  }
`

const Footer = () => {
  return (
    <FooterWrapper>
      <TransitionTop src={transitionTop} />
      <Container>
        <Menu>
          <Link to="/">home</Link>
          <Link to="/blog/what-is-skillcamp">about</Link>
          <a href="http://gitlab.com/skillcamp" target="_blank">
            projects
          </a>
          <Link to="/blog">blog</Link>
        </Menu>
        <Copyright>
          &copy; {new Date().getFullYear()} All rights reserved.
        </Copyright>
      </Container>
      <StripeRed />
      <StripeYellow />
      <StripeBlue />
    </FooterWrapper>
  )
}

export default Footer
